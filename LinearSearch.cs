using System;
using System.Collections.Generic;
using System.Text;

namespace LV7Z1i2
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double x)
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
                if (array[i] == x)
                    return i;
            return -1;
        }
    }
}
