using System;
using System.Collections.Generic;
using System.Text;

namespace LV7Z1i2
{
    abstract class SearchStrategy
    {
        public abstract int Search(double[] array, double x);
    }
}
