using System;

namespace LV7Z1i2
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence numberSequence = new NumberSequence(3);
            numberSequence.InsertAt(1, 2);
            numberSequence.InsertAt(0, 1);
            numberSequence.InsertAt(2, 3);
            numberSequence.Sort();
            numberSequence.Search(3);
        }
    }
}
